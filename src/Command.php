<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

\think\Console::addDefaultCommands(['\\ayhome\\crontab\\command\\Crontab']);

\think\Facade::bind([
    \ayhome\crontab\facade\Application::class => \ayhome\crontab\Application::class,
    \ayhome\crontab\facade\Crontab::class      => \ayhome\crontab\Crontab::class,
]);
